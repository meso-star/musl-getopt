# Musl getopt

Quick port of the [Musl](http://www.musl-libc.org/) getopt implementation for
the Microsoft CL compilers.

## How to build

This static library use the [CMake](http://www.cmake.org) toolchain. Ensure
that CMake is installed on your system and generate the project from the
`cmake/CMakeLists.txt` file. The resulting project can be edited, built and
installed as any CMake project.

## Licenses

The Musl library is copyright AC Rich Felker, et al. It is a free software
released under the standard MIT license. Its source code can be redistributed
under certain conditions; refer to the doc/musl/COPYRIGHT file for details.

The build system is copyright (c) |Meso|Star> and is released under the
[OSI](http://opensource.org)-approved CeCILL license. Refer to the COPYING
files for details.
