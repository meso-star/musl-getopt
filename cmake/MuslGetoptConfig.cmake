# Copyright (C) |Meso|Star> 2015 (contact@meso-star.com)
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty and the software's author, the holder of the
# economic rights, and the successive licensors have only limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading, using, modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate, and that also
# therefore means that it is reserved for developers and experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and, more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

cmake_minimum_required(VERSION 2.6)
# Try to find the MuslGetopt devel. Once done this will define:
#   - MuslGetopt_FOUND: system has MuslGetopt
#   - MuslGetoptINCLUDE_DIR: the include directory
#   - MuslGetopt: Link this to use musl-getopt
find_path(MuslGetopt_INCLUDE_DIR getopt.h)

# Look for Release, Debug, RelWithDebInfo and MinSizeRel libraries
unset(MuslGetopt_LIBRARY CACHE)
unset(MuslGetopt_LIBRARY_DEBUG CACHE)
unset(MuslGetopt_LIBRARY_RELWITHDEBINFO CACHE)
unset(MuslGetopt_LIBRARY_MINSIZEREL CACHE)

find_library(MuslGetopt_LIBRARY
  musl-getopt
  PATH_SUFFIXES bin Bin BIN
  DOC "Path to the library musl-getopt used during release builds.")
find_library(MuslGetopt_LIBRARY_DEBUG
  musl-getopt-dbg
  PATH_SUFFIXES bin Bin BIN
  DOC "Path to the library musl-getopt used during debug builds.")

# Release library fallback
if(NOT MuslGetopt_LIBRARY AND MuslGetopt_LIBRARY_DEBUG)
  get_property(_doc CACHE MuslGetopt_LIBRARY PROPERTY HELPSTRING)
  set(MuslGetopt_LIBRARY ${MuslGetopt_LIBRARY_DEBUG} CACHE PATH ${_doc} FORCE)
endif(NOT MuslGetopt_LIBRARY AND MuslGetopt_LIBRARY_DEBUG)

# Debug library fallback
if(NOT MuslGetopt_LIBRARY_DEBUG AND MuslGetopt_LIBRARY)
  get_property(_doc CACHE MuslGetopt_LIBRARY_DEBUG PROPERTY HELPSTRING)
  set(MuslGetopt_LIBRARY_DEBUG ${MuslGetopt_LIBRARY} CACHE PATH ${_doc} FORCE)
endif(NOT MuslGetopt_LIBRARY_DEBUG AND MuslGetopt_LIBRARY)

# Create the imported library target
if(CMAKE_HOST_WIN32)
  set(_property IMPORTED_IMPLIB)
else(CMAKE_HOST_WIN32)
  set(_property IMPORTED_LOCATION)
endif(CMAKE_HOST_WIN32)
add_library(MuslGetopt SHARED IMPORTED)
set_target_properties(MuslGetopt PROPERTIES
  ${_property} ${MuslGetopt_LIBRARY_DEBUG}
  ${_property}_DEBUG ${MuslGetopt_LIBRARY_DEBUG}
  ${_property}_RELEASE ${MuslGetopt_LIBRARY})

# Check the package
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(MuslGetopt DEFAULT_MSG
  MuslGetopt_INCLUDE_DIR
  MuslGetopt_LIBRARY
  MuslGetopt_LIBRARY_DEBUG)

