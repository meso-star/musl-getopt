# Copyright (C) |Meso|Star> 2015 (contact@meso-star.com)
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty and the software's author, the holder of the
# economic rights, and the successive licensors have only limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading, using, modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate, and that also
# therefore means that it is reserved for developers and experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and, more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

set(VERSION_MAJOR 1)
set(VERSION_MINOR 1)
set(VERSION_PATCH 9)
set(PACKAGE_VERSION "${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}")

if(NOT PACKAGE_FIND_VERSION
   OR PACKAGE_VERSION VERSION_EQUAL PACKAGE_FIND_VERSION)
  set(PACKAGE_VERSION_COMPATIBLE TRUE)
  set(PACKAGE_VERSION_EXACT TRUE)
  set(PACKAGE_VERSION_UNSUITABLE FALSE)
  return()
endif(NOT PACKAGE_FIND_VERSION
      OR PACKAGE_VERSION VERSION_EQUAL PACKAGE_FIND_VERSION)

if(NOT VERSION_MAJOR VERSION_EQUAL PACKAGE_FIND_VERSION_MAJOR)
  set(PACKAGE_VERSION_COMPATIBLE FALSE)
  set(PACKAGE_VERSION_EXACT FALSE)
  set(PACKAGE_VERSION_UNSUITABLE TRUE)
  return()
endif(NOT VERSION_MAJOR VERSION_EQUAL PACKAGE_FIND_VERSION_MAJOR)

if(VERSION_MINOR VERSION_LESS PACKAGE_FIND_VERSION_MINOR)
  set(PACKAGE_VERSION_COMPATIBLE FALSE)
  set(PACKAGE_VERSION_EXACT FALSE)
  set(PACKAGE_VERSION_UNSUITABLE TRUE)
  return()
endif(VERSION_MINOR VERSION_LESS PACKAGE_FIND_VERSION_MINOR)

if(VERSION_MINOR VERSION_EQUAL PACKAGE_FIND_VERSION_MINOR )
  if(VERSION_PATCH VERSION_LESS PACKAGE_FIND_VERSION_PATCH)
    set(PACKAGE_VERSION_COMPATIBLE FALSE)
    set(PACKAGE_VERSION_EXACT FALSE)
    set(PACKAGE_VERSION_UNSUITABLE TRUE)
    return()
  endif()
endif()

set(PACKAGE_VERSION_COMPATIBLE TRUE)
set(PACKAGE_VERSION_EXACT FALSE)
set(PACKAGE_VERSION_UNSUITABLE FALSE)

