#ifndef MUSL_GETOPT_H
#define MUSL_GETOPT_H

#ifdef __cplusplus
extern "C" {
#endif

int getopt(int, char * const [], const char *);
extern char *optarg;
extern int optind, opterr, optopt;

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* MUSL_GET_OPT */

